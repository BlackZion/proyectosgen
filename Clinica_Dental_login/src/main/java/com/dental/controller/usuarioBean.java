/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dental.controller;

import com.dental.impl.Usuarioimpl;
import java.io.Serializable;
import com.dental.models.RolUsuario;
import java.util.List;
import com.dental.models.Usuario;
import java.io.IOException;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

@ManagedBean(name = "usuarioBean")
@SessionScoped
public class usuarioBean {

    private Usuarioimpl impl;
    private Usuario user;
    private List<Usuario> lista;
    private Usuario usuario1;
    private RolUsuario roluser;

    String Mensaje;

    public Usuario getUser() {
        return user;
    }

    public void setUser(Usuario user) {
        this.user = user;
    }

    public List<Usuario> getLista() {
        return lista;
    }

    public void setLista(List<Usuario> lista) {
        this.lista = lista;
    }

    public Usuario getUsuario1() {
        return usuario1;
    }

    public void setUsuario1(Usuario usuario1) {
        this.usuario1 = usuario1;
    }

    public RolUsuario getRoluser() {
        return roluser;
    }

    public void setRoluser(RolUsuario roluser) {      
        this.roluser = roluser;
    }

    @PostConstruct

    public void init() {
        user = new Usuario();
        impl = new Usuarioimpl();
        roluser = new RolUsuario();
        usuario1 = new Usuario();

    }

    public void findAll() {
        try {
            this.lista = impl.findAll();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void create() {
        try {
            impl.create(user);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void findById(Usuario us) {
        roluser.setId(us.getId_rol().getId());
        this.usuario1 = us;
    }

    public String login() throws IOException {
        String ruta = null;
        try {
            usuario1 = impl.login(user);
           
            user = new Usuario();
            
            roluser.setId(usuario1.getId_rol().getId());
            roluser.setDescripcion(usuario1.getId_rol().getDescripcion());
            
            this.usuario1.setId_rol(roluser);
            
            if (usuario1 == null) {
                this.Mensaje = "Usuario o Contraseña Incorrecto";
            } else {
                /* if ("Admin".equals(usuario1.getId_rol().getDescripcion())) {
                    FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("user", usuario1.getUsuario());
                    FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("tipo", usuario1.getId_rol());
                    ruta = "empresa.xhtml";                                       
                } *///else {
                FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("roluser", usuario1.getId_rol());
                FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("user", usuario1.getUsuario());
                ruta = "empresa.xhtml?faces-redirect=true";

                // }
            }
        } catch (Exception e) {
            System.out.println("Fallo");
            FacesContext.getCurrentInstance().addMessage(
                    null,
                    new FacesMessage(FacesMessage.SEVERITY_WARN,
                            "Usuario o Contraseña Incorrecto",
                            "Porfavor Ingresa la contraseña y usuario Correcto"));;

            //FacesContext.getCurrentInstance().getExternalContext().redirect("Login.xhtml");
            return "Login";

        }
        //findbyid(usuario1);
        //  this.usuario1.setId_rol(roluser);
        return ruta;
    }

    public void verificarSesion() {
        FacesContext context = FacesContext.getCurrentInstance();
        try {
            Usuario us = (Usuario) context.getExternalContext().getSessionMap().get("user");
            if (us == null) {
                context.getExternalContext().redirect("Login.xhtml");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
