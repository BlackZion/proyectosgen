/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dental.controller;

import com.dental.impl.Empresaimpl;
import com.dental.models.Empresa;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@ManagedBean(name = "empresaBean")
@SessionScoped
public class empresaBean {
    
    private Empresaimpl impl;
    private Empresa empresa;
    private List<Empresa> lista;
    
    public Empresa getEmpresa() {
        return empresa;
    }
    
    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }
    
    public List<Empresa> getLista() {
      
        return lista;
    }
    
    public void setLista(List<Empresa> lista) {
        this.lista = lista;
    }
    
    @PostConstruct
    public void init() {
        empresa = new Empresa();
        impl = new Empresaimpl();
    }
    
    public void findAll() {
        try {
            this.lista = impl.findAll();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public void save() {
        try {
            impl.create(empresa);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public void edit(){
        try {
            this.impl.edit(empresa);
            findAll();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void remove(Empresa emp){
        try {
            impl.remove(emp);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public void findById(Empresa em){
        this.empresa = em;
    }
}
