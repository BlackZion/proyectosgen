/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dental.util;
import java.util.List;


public interface Dao<T> {
        
    void create(T  t);        
    
    List<T> findAll();

    void edit(T t);
    
    void remove(T t);
}
