/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dental.impl;

import com.dental.models.Empresa;
import javax.persistence.EntityManager;
import com.dental.util.AbstractFacade;
import javax.persistence.EntityManagerFactory;
import javax.persistence.*;
import com.dental.util.Dao;

public class Empresaimpl extends AbstractFacade<Empresa> implements Dao<Empresa> {

    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public Empresaimpl() {
        super(Empresa.class);
        EntityManagerFactory emf;
        emf = Persistence.createEntityManagerFactory("sec_dental");
        em = emf.createEntityManager();
    }
}
