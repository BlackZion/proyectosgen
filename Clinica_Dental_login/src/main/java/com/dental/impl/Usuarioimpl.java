/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dental.impl;

import com.dental.models.RolUsuario;
import com.dental.models.Usuario;
import com.dental.models.Usuario_;
import javax.persistence.EntityManager;
import com.dental.util.AbstractFacade;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import com.dental.util.Dao;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.persistence.Query;

public class Usuarioimpl extends AbstractFacade<Usuario> implements Dao<Usuario> {

    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public Usuarioimpl() {
        super(Usuario.class);
        EntityManagerFactory emf;
        emf = Persistence.createEntityManagerFactory("sec_dental");
        em = emf.createEntityManager();
    }

    public Usuario login(Usuario usuario) {
        em.getTransaction().begin();        
     
        System.out.println("User:" + usuario.getUsuario());
        System.out.println("Pass:" + usuario.getClave());
        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery cq = cb.createQuery();
            Root<Usuario> root = cq.from(Usuario.class);
            cq.where(
                    cb.equal(root.get(Usuario_.usuario), usuario.getUsuario()),
                    cb.and(cb.equal(root.get(Usuario_.clave), usuario.getClave())                    
                    ));
            Query q = em.createQuery(cq);
            usuario = (Usuario) q.getSingleResult();
            System.out.println("Que es?"+usuario);
            if (null == usuario) {
                usuario = null;
            } else {                
                System.out.println("Porque aqui?"+usuario.getId());
                return usuario;
            }
        } catch (Exception e) {            
            usuario = null;
        }
        
        em.getTransaction().commit();
        return usuario;
    }
    

}
