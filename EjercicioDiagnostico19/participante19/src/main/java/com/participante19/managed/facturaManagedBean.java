/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.participante19.managed;

import com.participante19.ejb.facturaFacadeLocal;
import com.participante19.entity.cliente;
import com.participante19.entity.factura;
import com.participante19.entity.modo_pago;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;

/**
 *
 * @author rodrigo.martinezusam
 */
@Named(value = "facturaManagedBean")
@SessionScoped
public class facturaManagedBean implements Serializable {

    @EJB
    private facturaFacadeLocal facturaejb;
    private factura factura;
    private cliente cliente;
    private modo_pago modo_pago;
    private List<factura> lista;

    String Mensaje;

    public factura getFactura() {
        return factura;
    }

    public void setFactura(factura factura) {
        this.factura = factura;
    }

    public List<factura> getLista() {
        this.lista = facturaejb.findAll();
        return lista;
    }

    public void setLista(List<factura> lista) {
        this.lista = lista;
    }

    public String getMensaje() {
        return Mensaje;
    }

    public void setMensaje(String Mensaje) {
        this.Mensaje = Mensaje;
    }

    public cliente getCliente() {
        return cliente;
    }

    public void setCliente(cliente cliente) {
        this.cliente = cliente;
    }

    public modo_pago getModo_pago() {
        return modo_pago;
    }

    public void setModo_pago(modo_pago modo_pago) {
        this.modo_pago = modo_pago;
    }

    @PostConstruct

    public void init() {
        factura = new factura();
        cliente = new cliente();
        modo_pago = new modo_pago();
    }

    public void save() {
        try {
            factura.setId_cliente(cliente);
            factura.setNum_pago(modo_pago);
            facturaejb.create(factura);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void update() {
        try {
            factura.setId_cliente(cliente);
            factura.setNum_pago(modo_pago);
            facturaejb.edit(factura);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void remove(factura fac) {
        try {
            facturaejb.remove(fac);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void findByid(factura fa) {
        cliente.setId_cliente(fa.getId_cliente().getId_cliente());
        modo_pago.setNum_pago(fa.getNum_pago().getNum_pago());
        this.factura = fa;
    }

}
