/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.participante19.managed;

import com.participante19.ejb.modo_pagoFacadeLocal;
import com.participante19.entity.modo_pago;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;

/**
 *
 * @author rodrigo.martinezusam
 */
@Named(value = "modo_pagoManagedBean")
@SessionScoped
public class modo_pagoManagedBean implements Serializable {

    @EJB
    private modo_pagoFacadeLocal modo_ejb;
    private modo_pago modo_pago;
    private List<modo_pago> lista;

    String mensaje;

    public modo_pago getModo_pago() {
        return modo_pago;
    }

    public void setModo_pago(modo_pago modo_pago) {
        this.modo_pago = modo_pago;
    }

    public List<modo_pago> getLista() {
        this.lista = modo_ejb.findAll();
        return lista;
    }

    public void setLista(List<modo_pago> lista) {
        this.lista = lista;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    @PostConstruct
    public void init() {
        modo_pago = new modo_pago();
    }

    public void save() {
        try {
            modo_ejb.create(modo_pago);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void update() {
        try {
            modo_ejb.edit(modo_pago);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void remove(modo_pago mo) {
        try {
            modo_ejb.remove(mo);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void findbyid(modo_pago m) {
        this.modo_pago = m;
    }

}
