/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.participante19.managed;

import com.participante19.ejb.categoriaFacadeLocal;
import com.participante19.entity.categoria;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;


@Named(value = "categoriaManagedBean")
@SessionScoped
public class categoriaManagedBean implements Serializable {

    @EJB
    private categoriaFacadeLocal categoriaejb;
    private categoria categoria;
    private List<categoria> lista;

    String Mensaje;

    public categoria getCategoria() {
        return categoria;
    }

    public void setCategoria(categoria categoria) {
        this.categoria = categoria;
    }

    public List<categoria> getLista() {
        this.lista = categoriaejb.findAll();
        return lista;
    }

    public void setLista(List<categoria> lista) {
        this.lista = lista;
    }

    @PostConstruct

    public void init() {
        categoria = new categoria();
    }

    public void save() {
        try {
            categoriaejb.create(categoria);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void update() {
        try {
            categoriaejb.edit(categoria);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void remove(categoria cat) {
        try {
            categoriaejb.remove(cat);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public void findbyid(categoria ca){
        this.categoria = ca;
    }

}
