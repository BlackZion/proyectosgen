/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.participante19.managed;

import com.participante19.ejb.productoFacadeLocal;
import com.participante19.entity.categoria;
import com.participante19.entity.producto;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;

/**
 *
 * @author rodrigo.martinezusam
 */
@Named(value = "productoManagedBean")
@SessionScoped
public class productoManagedBean implements Serializable {

    @EJB
    private productoFacadeLocal productoejb;
    private producto producto;
    private categoria categoria;
    private List<producto> lista;

    String Mensaje;

    public producto getProducto() {
        return producto;
    }

    public void setProducto(producto producto) {
        this.producto = producto;
    }

    public List<producto> getLista() {
        this.lista = productoejb.findAll();
        return lista;
    }

    public void setLista(List<producto> lista) {
        this.lista = lista;
    }

    public categoria getCategoria() {
        return categoria;
    }

    public void setCategoria(categoria categoria) {
        this.categoria = categoria;
    }

    @PostConstruct

    public void init() {
        producto = new producto();
        categoria = new categoria();
    }

    public void save() {
        try {
            producto.setId_categoria(categoria);
            productoejb.create(producto);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void update() {
        try {
            producto.setId_categoria(categoria);
            productoejb.edit(producto);
        } catch (Exception e) {
        }
    }

    public void remove(producto pro) {
        try {
            productoejb.remove(pro);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void findByid(producto p) {
        categoria.setId_categoria(p.getId_categoria().getId_categoria());
        this.producto = p;
    }

}
