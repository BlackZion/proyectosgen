/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.participante19.ejb;

import com.participante19.entity.producto;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author rodrigo.martinezusam
 */
@Local
public interface productoFacadeLocal {

    void create(producto producto);

    void edit(producto producto);

    void remove(producto producto);

    List<producto> findAll();
}
