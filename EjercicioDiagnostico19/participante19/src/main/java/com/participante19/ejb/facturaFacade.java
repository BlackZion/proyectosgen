/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.participante19.ejb;

import com.participante19.entity.factura;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author rodrigo.martinezusam
 */
@Stateless
public class facturaFacade extends AbstractFacade<factura> implements facturaFacadeLocal {

    @PersistenceContext(unitName = "persistencia")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public facturaFacade() {
        super(factura.class);
    }
    
}
