package com.participante19.entity;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.0.v20130507-rNA", date="2019-10-14T14:54:20")
@StaticMetamodel(cliente.class)
public class cliente_ { 

    public static volatile SingularAttribute<cliente, Integer> id_cliente;
    public static volatile SingularAttribute<cliente, String> apellido;
    public static volatile SingularAttribute<cliente, String> direccion;
    public static volatile SingularAttribute<cliente, Date> fecha_nacimiento;
    public static volatile SingularAttribute<cliente, String> telefono;
    public static volatile SingularAttribute<cliente, String> nombre;
    public static volatile SingularAttribute<cliente, String> email;

}