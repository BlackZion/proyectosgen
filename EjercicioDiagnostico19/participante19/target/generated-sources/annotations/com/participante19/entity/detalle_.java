package com.participante19.entity;

import com.participante19.entity.factura;
import com.participante19.entity.producto;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.0.v20130507-rNA", date="2019-10-14T14:54:20")
@StaticMetamodel(detalle.class)
public class detalle_ { 

    public static volatile SingularAttribute<detalle, factura> id_factura;
    public static volatile SingularAttribute<detalle, Double> precio;
    public static volatile SingularAttribute<detalle, producto> id_producto;
    public static volatile SingularAttribute<detalle, Integer> cantidad;
    public static volatile SingularAttribute<detalle, Integer> num_detalle;

}