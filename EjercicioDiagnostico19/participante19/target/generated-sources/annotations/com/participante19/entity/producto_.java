package com.participante19.entity;

import com.participante19.entity.categoria;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.0.v20130507-rNA", date="2019-10-14T14:54:20")
@StaticMetamodel(producto.class)
public class producto_ { 

    public static volatile SingularAttribute<producto, Double> precio;
    public static volatile SingularAttribute<producto, categoria> id_categoria;
    public static volatile SingularAttribute<producto, Integer> id_producto;
    public static volatile SingularAttribute<producto, Integer> stock;
    public static volatile SingularAttribute<producto, String> nombre;

}