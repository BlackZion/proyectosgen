package com.reportes.models;
// Generated 12-18-2019 10:28:22 AM by Hibernate Tools 5.1.10.Final

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Producto generated by hbm2java
 */
@Entity
@Table(name = "producto", catalog = "facturacion")
public class Producto implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer idProducto;
	private Categoria categoria;
	private String nombre;
	private double precio;
	private int stock;
	private Set<Detalle> detalles = new HashSet<Detalle>(0);

	public Producto() {
	}

	public Producto(Categoria categoria, String nombre, double precio, int stock) {
		this.categoria = categoria;
		this.nombre = nombre;
		this.precio = precio;
		this.stock = stock;
	}

	public Producto(Categoria categoria, String nombre, double precio, int stock, Set<Detalle> detalles) {
		this.categoria = categoria;
		this.nombre = nombre;
		this.precio = precio;
		this.stock = stock;
		this.detalles = detalles;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "id_producto", unique = true, nullable = false)
	public Integer getIdProducto() {
		return this.idProducto;
	}

	public void setIdProducto(Integer idProducto) {
		this.idProducto = idProducto;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_categoria", nullable = false)
	public Categoria getCategoria() {
		return this.categoria;
	}

	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}

	@Column(name = "nombre", nullable = false, length = 15)
	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Column(name = "precio", nullable = false, precision = 22, scale = 0)
	public double getPrecio() {
		return this.precio;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}

	@Column(name = "stock", nullable = false)
	public int getStock() {
		return this.stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "producto")
	public Set<Detalle> getDetalles() {
		return this.detalles;
	}

	public void setDetalles(Set<Detalle> detalles) {
		this.detalles = detalles;
	}

}
