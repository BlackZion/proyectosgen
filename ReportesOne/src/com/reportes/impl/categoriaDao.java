package com.reportes.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.reportes.models.Categoria;

@Repository
@Transactional
public class categoriaDao implements categoriaimpl{

	
	@Autowired
	private SessionFactory sessionFactory;

	public Session getSession() {
		return sessionFactory.getCurrentSession();
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Categoria> findCat(){
		Query<Categoria> query = getSession().createQuery("from Categoria");		
		return query.list();		
	}
	
}
