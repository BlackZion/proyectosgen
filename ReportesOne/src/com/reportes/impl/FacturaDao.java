
package com.reportes.impl;



import org.springframework.transaction.annotation.Transactional;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public class FacturaDao implements facturaimpl {

	@Autowired
	private SessionFactory sessionfactory;

	public Session getSession() {
		return sessionfactory.getCurrentSession();
	}


}
