package com.reportes.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.reportes.models.Categoria;
import com.reportes.models.Producto;

@Transactional
@Repository
public class ProductosDao implements Productosimpl {

	@Autowired
	private SessionFactory SessionFactory;

	private Session getSession() {
		return SessionFactory.getCurrentSession();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Producto> find() {
		Query<Producto> query = getSession().createQuery("from Producto");
		return query.list();
	}

	@Override
	public List<Producto> espList(int ref) {
		List<Producto> p = new ArrayList<>();
		try {
			
			CriteriaBuilder cb = getSession().getCriteriaBuilder();
			CriteriaQuery<Producto> cq = cb.createQuery(Producto.class);
			Root<Producto> root = cq.from(Producto.class);
			Join<Producto, Categoria> join = root.join("categoria");
			cq.where(cb.equal(root.get("categoria"), ref));

			p = getSession().createQuery(cq).getResultList();
			System.out.println("Datos obtenidos ----------------------> "+p.size());
						
		} catch (Exception e) {
			e.printStackTrace();
			return p;
		}

		return p;
	}

}
