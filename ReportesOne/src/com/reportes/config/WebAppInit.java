package com.reportes.config;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;

import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

import com.sun.faces.config.FacesInitializer;

public class WebAppInit extends FacesInitializer implements WebApplicationInitializer{

	@Override
	public void onStartup(ServletContext servletContext) throws ServletException {
	AnnotationConfigWebApplicationContext config = new AnnotationConfigWebApplicationContext();
	config.register(AppContextConfig.class);
	ServletRegistration.Dynamic dispatcher = servletContext.addServlet("SpringDispatcher", new DispatcherServlet(config));
	dispatcher.setLoadOnStartup(1);
	dispatcher.addMapping("/");
	ContextLoaderListener contextLoaderListener = new ContextLoaderListener(config); 
	servletContext.addListener(contextLoaderListener);
		
	}

}
