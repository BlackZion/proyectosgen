package com.reportes.ctrl;

import java.io.Serializable;
import java.sql.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.SessionScope;

import com.reportes.impl.Productosimpl;
import com.reportes.impl.categoriaimpl;
import com.reportes.models.Categoria;
import com.reportes.models.Producto;

@Component
@SessionScope
@ManagedBean
public class ReporteBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Autowired
	@Qualifier("producto")
	private Productosimpl productoimpl;
	private Producto producto;
	private List<Producto> listProducto;

	@Autowired
	@Qualifier("categoria")
	private categoriaimpl categoriaimpl;
	private Categoria categoria;
	private List<Categoria> liscategoria;

	/* Util */
	private Date Dateref;
	private int ref;

	public Date getDateref() {
		return Dateref;
	}

	public void setDateref(Date dateref) {
		Dateref = dateref;
	}

	public Producto getProducto() {
		return producto;
	}

	public void setProducto(Producto producto) {
		this.producto = producto;
	}

	public List<Producto> getListProducto() {
		this.listProducto = productoimpl.find();
		return listProducto;
	}

	public void setListProducto(List<Producto> listProducto) {
		this.listProducto = listProducto;
	}

	public Categoria getCategoria() {
		return categoria;
	}

	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}

	public List<Categoria> getLiscategoria() {
		this.liscategoria = categoriaimpl.findCat();
		return liscategoria;
	}

	public void setLiscategoria(List<Categoria> liscategoria) {
		this.liscategoria = liscategoria;
	}

	public int getRef() {
		return ref;
	}

	public void setRef(int ref) {
		this.ref = ref;
	}

	@PostConstruct
	public void init() {
		producto = new Producto();
		ref = new Integer(0);
	}

	
	public void Esp() {
		try {
			System.out.println("REF = " + ref);			
              
              if(ref == 0) {
            	  FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
              "Error","Categoria inexistente"));            	  
              }else {
            	 this.listProducto =  productoimpl.espList(ref);  
            	             	
              }
              
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
