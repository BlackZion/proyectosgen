package com.blackzion.models;
// Generated 12-17-2019 09:47:53 AM by Hibernate Tools 5.2.12.Final

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * ClienteServicios generated by hbm2java
 */
@Entity
@Table(name = "cliente_servicios", catalog = "Ejercicio_Practico")
public class ClienteServicios implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer idClienteservicio;
	private Clientes idClientes;
	private Servicios idServicios;

	public ClienteServicios() {
	}

	public ClienteServicios(Clientes idClientes, Servicios idServicios) {
		this.idClientes = idClientes;
		this.idServicios = idServicios;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "idClienteservicio", unique = true, nullable = false)
	public Integer getIdClienteservicio() {
		return this.idClienteservicio;
	}

	public void setIdClienteservicio(Integer idClienteservicio) {
		this.idClienteservicio = idClienteservicio;
	}

	@Column(name = "idClientes")
	public Clientes getIdClientes() {
		return this.idClientes;
	}

	public void setIdClientes(Clientes idClientes) {
		this.idClientes = idClientes;
	}

	@Column(name = "idServicios")
	public Servicios getIdServicios() {
		return this.idServicios;
	}

	public void setIdServicios(Servicios idServicios) {
		this.idServicios = idServicios;
	}

}
