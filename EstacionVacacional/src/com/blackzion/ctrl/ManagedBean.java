package com.blackzion.ctrl;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;

import org.primefaces.event.FlowEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.SessionScope;
import org.springframework.beans.factory.annotation.Qualifier;

import com.blackzion.Dao.ClientesDao;
import com.blackzion.Dao.ClienteservicioDao;
import com.blackzion.Dao.FacturaDao;
import com.blackzion.Dao.PacksVacacionalesDao;
import com.blackzion.Dao.TPaquetesVacacionalesDao;
import com.blackzion.Dao.ServiciosDao;
import com.blackzion.models.ClienteServicios;
import com.blackzion.models.Clientes;
import com.blackzion.models.Factura;
import com.blackzion.models.PaquetesVacacionales;
import com.blackzion.models.Servicios;
import com.blackzion.models.TipoPaquetesvacacionales;

@Component
@SessionScope
@javax.faces.bean.ManagedBean
public class ManagedBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Autowired
	@Qualifier("facturaimpl")
	private FacturaDao DaoFactura;
	private Factura factura;
	private List<Factura> listFacturas;

	@Autowired
	@Qualifier("cliente")
	private ClientesDao clienteDao;
	private Clientes cliente;
	private List<Clientes> listCl;

	@Autowired
	@Qualifier("servicioDao")
	private ServiciosDao servicioDao;
	private Servicios servicio;
	private List<Servicios> listser;

	@Autowired
	@Qualifier("tpaquetesVacacionales")
	private TPaquetesVacacionalesDao TpaquetesVacacionalesDao;
	private TipoPaquetesvacacionales TpaquetesVacacionales;
	private List<TipoPaquetesvacacionales> lisTipopaquetes;

	@Autowired
	@Qualifier("pkvacacional")
	private PacksVacacionalesDao packvacacionalesDao;
	private PaquetesVacacionales paquetesVacacionales;
	private List<PaquetesVacacionales> listpaquete;

	@Autowired
	@Qualifier("clienteServicios")
	private ClienteservicioDao clienteservicioDao;
	private ClienteServicios clienteServicios;
	private List<ClienteServicios> listServicioscliente;

	// Util
	private boolean skip;
	private String ruta;

	private int estimado;

	public Factura getFactura() {
		return factura;
	}

	public void setFactura(Factura factura) {
		this.factura = factura;
	}

	public List<Factura> getListFacturas() {
		return listFacturas;
	}

	public void setListFacturas(List<Factura> listFacturas) {
		this.listFacturas = listFacturas;
	}

	public boolean isSkip() {
		return skip;
	}

	public void setSkip(boolean skip) {
		this.skip = skip;
	}

	public Clientes getCliente() {
		return cliente;
	}

	public void setCliente(Clientes cliente) {
		this.cliente = cliente;
	}

	public List<Clientes> getListCl() {
		return listCl;
	}

	public void setListCl(List<Clientes> listCl) {
		this.listCl = listCl;
	}

	public Servicios getServicio() {
		return servicio;
	}

	public void setServicio(Servicios servicio) {
		this.servicio = servicio;
	}

	public List<Servicios> getListser() {
		this.listser = servicioDao.obtenerServicios();
		return listser;
	}

	public void setListser(List<Servicios> listser) {
		this.listser = listser;
	}

	public TipoPaquetesvacacionales getTpaquetesVacacionales() {
		return TpaquetesVacacionales;
	}

	public void setTpaquetesVacacionales(TipoPaquetesvacacionales tpaquetesVacacionales) {
		TpaquetesVacacionales = tpaquetesVacacionales;
	}

	public List<TipoPaquetesvacacionales> getLisTipopaquetes() {
		this.lisTipopaquetes = TpaquetesVacacionalesDao.obtenerTPaqueteVacacional();
		return lisTipopaquetes;
	}

	public void setLisTipopaquetes(List<TipoPaquetesvacacionales> lisTipopaquetes) {
		this.lisTipopaquetes = lisTipopaquetes;
	}

	public String getRuta() {
		return ruta;
	}

	public void setRuta(String ruta) {
		this.ruta = ruta;
	}

	public PaquetesVacacionales getPaquetesVacacionales() {
		return paquetesVacacionales;
	}

	public void setPaquetesVacacionales(PaquetesVacacionales paquetesVacacionales) {
		this.paquetesVacacionales = paquetesVacacionales;
	}

	public List<PaquetesVacacionales> getListpaquete() {
		return listpaquete;
	}

	public void setListpaquete(List<PaquetesVacacionales> listpaquete) {
		this.listpaquete = listpaquete;
	}

	public ClienteServicios getClienteServicios() {
		return clienteServicios;
	}

	public void setClienteServicios(ClienteServicios clienteServicios) {
		this.clienteServicios = clienteServicios;
	}

	public List<ClienteServicios> getListServicioscliente() {
		this.listServicioscliente = clienteservicioDao.obtenerClienteservicio();
		return listServicioscliente;
	}

	public void setListServicioscliente(List<ClienteServicios> listServicioscliente) {
		this.listServicioscliente = listServicioscliente;
	}

	public int getEstimado() {
		return estimado;
	}

	public void setEstimado(int estimado) {
		this.estimado = estimado;
	}

	@PostConstruct
	public void init() {
		factura = new Factura();
		cliente = new Clientes();
		servicio = new Servicios();
		TpaquetesVacacionales = new TipoPaquetesvacacionales();
		paquetesVacacionales = new PaquetesVacacionales();

	}

	public void findAll() {
		try {
			this.listFacturas = DaoFactura.obtenerFactura();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public String onFlowProcess(FlowEvent event) {
		if (skip) {
			skip = false; // reset in case user goes back
			return "confirm";
		} else {
			return event.getNewStep();
		}
	}

	public String redirect() {
		ruta = "servicio.xhtml?faces-redirect=true";
		return ruta;
	}

	public String findproventa(TipoPaquetesvacacionales tva) {
		try {
			this.TpaquetesVacacionales = tva;
			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("Tpack", TpaquetesVacacionales);
			ruta = "main.xhtml?faces-redirect=true";

		} catch (Exception e) {
			e.printStackTrace();
		}
		return ruta;

	}

	public void saveProcess() {
		try {
			 clienteDao.saveClientes(cliente); 

			SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
			Date date = new Date();

			String fechap = formatter.format(paquetesVacacionales.getCantidadDiasI());
			String fechaF = formatter.format(paquetesVacacionales.getCantidadDiasF()); 

			Date date1 = formatter.parse(fechaF);
			Date date2 = formatter.parse(fechap);


			Calendar day1 = Calendar.getInstance();
			Calendar day2 = Calendar.getInstance();
			day1.setTime(date1);
			day2.setTime(date2);

			int contador = day1.get(Calendar.DAY_OF_YEAR) - day2.get(Calendar.DAY_OF_YEAR);

			double valor = contador * TpaquetesVacacionales.getCosto();

			paquetesVacacionales.setTotalPaquete(valor);
			paquetesVacacionales.setIdTipoPack(TpaquetesVacacionales.getIdPaquetesVacacionales());
			packvacacionalesDao.savePackVacacionales(paquetesVacacionales);
			
			
			factura.setIdClienteSer(cliente.getIdClientes());
			factura.setIdPackVacacional(paquetesVacacionales.getIdPackVacacional());
			DaoFactura.saveFactura(factura);
			

		} catch (Exception e) {
             e.printStackTrace();
		}
	}

	
	public String cancelar() {
		 ruta = "index.xhtml?faces-redirect=true";
		
		 return ruta;
	}
	
}
