package com.blackzion.ctrl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Named;

import org.springframework.web.context.annotation.RequestScope;

@Named
@RequestScope
public class imagenesBean {

    private List<String> images;
    
    @PostConstruct
    public void init() {
        images = new ArrayList<String>();
        for (int i = 2; i <= 5; i++) {
            images.add("hotel" + i + ".jpg");
        }
    }
 
    public List<String> getImages() {
        return images;
    }
}
