package com.blackzion.Dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.blackzion.models.PaquetesVacacionales;
import com.blackzion.models.TipoPaquetesvacacionales;


@SuppressWarnings("unchecked")
@Transactional
@Repository
public class TPaquetesVacacionalesImpl implements TPaquetesVacacionalesDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	public Session getSession() {
		return sessionFactory.getCurrentSession();
	}
	
	@SuppressWarnings("rawtypes")
	@Override
	public List<TipoPaquetesvacacionales> obtenerTPaqueteVacacional() {
		Query query = getSession().createQuery("from TipoPaquetesvacacionales");
			return query.list();
	}

	@Override
	public void savePaquetesVacacionales(TipoPaquetesvacacionales paV) {
			getSession().save(paV);
	}

	@SuppressWarnings("deprecation")
	@Override
	public TipoPaquetesvacacionales findByIdPackVacacional(int idPackVacacional) {
		Criteria crit = getSession().createCriteria(TipoPaquetesvacacionales.class);
		crit.add(Restrictions.eq("idPaquetesVacacionales", idPackVacacional));
			return (TipoPaquetesvacacionales) crit.uniqueResult();
	}

	@Override
	public void updatePaquetes(TipoPaquetesvacacionales paV) {
			getSession().update(paV);
	}

	@Override
	public void deletePaquetes(TipoPaquetesvacacionales paV) {
			getSession().delete(paV);
	}

}
