package com.blackzion.Dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.hibernate.query.Query;

import com.blackzion.models.Factura;


@Transactional
@Repository
public class FacturaImpl implements FacturaDao{

	@Autowired
	private SessionFactory sessionFactory;
	
	public Session getSession() {
		return sessionFactory.getCurrentSession();
	}
	
	@Override
	public List<Factura> obtenerFactura() {		
		Query<Factura> query = getSession().createQuery("from Factura",Factura.class);
		return query.list();		
	}
	

	@Override
	public void saveFactura(Factura fac) {
	 getSession().save(fac);
		
	}

	
	@SuppressWarnings("deprecation")
	@Override
	public Factura findByIdFactura(int idfactura) {
         Criteria crit = getSession().createCriteria(Factura.class);
         crit.add(Restrictions.eq("idClientes", idfactura));
		return (Factura) crit.uniqueResult();
	}

	@Override
	public void updateFactura(Factura fac) {
	getSession().update(fac);
		
	}

	@Override
	public void deleteFactura(Factura fac) {
		getSession().delete(fac);
		
	}

}
