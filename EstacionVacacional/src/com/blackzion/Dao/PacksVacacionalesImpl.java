package com.blackzion.Dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.blackzion.models.PaquetesVacacionales;

@SuppressWarnings("unchecked")
@Transactional
@Repository
public class PacksVacacionalesImpl implements PacksVacacionalesDao{

	@Autowired
	private SessionFactory sessionFactory;
	
	public Session getSession() {
		return sessionFactory.getCurrentSession();
	}
	
	@Override
	public List<PaquetesVacacionales> obtenerPacks() {
		Query<PaquetesVacacionales> query = getSession().createQuery("from paquetes_vacacionales");
		return query.list();
	}

	@Override
	public void savePackVacacionales(PaquetesVacacionales pac) {
		   getSession().save(pac);	
	}

	@SuppressWarnings("deprecation")
	@Override
	public PaquetesVacacionales findByIdPackVacacional(int idPackVacacional) {
		Criteria crit = getSession().createCriteria(PaquetesVacacionales.class);
		crit.add(Restrictions.eq("idPaquetesVacacionales", idPackVacacional));
		 return (PaquetesVacacionales) crit.uniqueResult();
	}

	@Override
	public void updatePaquetesVacacionales(PaquetesVacacionales pac) {
			getSession().update(pac);
	}

	@Override
	public void deletePaquetesVacacionales(PaquetesVacacionales pac) {
			getSession().delete(pac);
	}

}
