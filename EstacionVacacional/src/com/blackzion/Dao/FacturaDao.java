package com.blackzion.Dao;

import java.util.List;

import com.blackzion.models.Factura;

public interface FacturaDao {
	public List<Factura> obtenerFactura();
	
	public void saveFactura (Factura fac);
	
	public void updateFactura (Factura fac);
	public void deleteFactura (Factura fac);

	Factura findByIdFactura(int fac);
}
