package com.blackzion.Dao;

import java.util.List;

import com.blackzion.models.ClienteServicios;

public interface ClienteservicioDao {
			
	public List<ClienteServicios> obtenerClienteservicio();
	
	public void saveClienteservicio (ClienteServicios clser);
	public ClienteServicios findByidClienteservicio (int idClienteServicio);
	
	public void updateClienteservicio(ClienteServicios clser);
	public void deleteClienteservicio(ClienteServicios clser);
}
