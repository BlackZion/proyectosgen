package com.blackzion.Dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.blackzion.models.Clientes;

@SuppressWarnings("unchecked")
@Transactional
@Repository
public class ClientesImpl implements ClientesDao{
	@Autowired
	private SessionFactory sessionFactory;
	
	public Session getSession() {
		return sessionFactory.getCurrentSession();
	}
	
	@Override
	public List<Clientes> obtenerClientes() {
		Query<Clientes> query = getSession().createQuery("from Clientes");
		return  query.list();
	}

	@Override
	public void saveClientes(Clientes cli) {
		getSession().save(cli);
		
	}

	@SuppressWarnings("deprecation")
	@Override
	public Clientes findByidClientes(int id_clientes) {
		Criteria crit = getSession().createCriteria(Clientes.class);
		crit.add(Restrictions.eq("idClientes", id_clientes));
		return (Clientes) crit.uniqueResult();
		
	}

	@Override
	public void updateClientes(Clientes cli) {
		getSession().update(cli);
		
	}

	@Override
	public void deleteClientes(Clientes clie) {
		getSession().delete(clie);
		
	}

}
