package com.blackzion.Dao;

import java.util.List;


import com.blackzion.models.TipoPaquetesvacacionales;

public interface TPaquetesVacacionalesDao {
	public List<TipoPaquetesvacacionales> obtenerTPaqueteVacacional();
	
	public void savePaquetesVacacionales (TipoPaquetesvacacionales paV);
	public TipoPaquetesvacacionales findByIdPackVacacional(int idPackVacacional);
	
	public void updatePaquetes (TipoPaquetesvacacionales paV);
	public void deletePaquetes (TipoPaquetesvacacionales paV);
}
