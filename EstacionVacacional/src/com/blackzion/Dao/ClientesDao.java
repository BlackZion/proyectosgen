package com.blackzion.Dao;

import java.util.List;
import com.blackzion.models.Clientes;

public interface ClientesDao {
	public List<Clientes> obtenerClientes();
	
	public void saveClientes (Clientes cli);
	public Clientes findByidClientes (int idClientes);
	
	public void updateClientes(Clientes cli);
	public void deleteClientes(Clientes cli);
}
