package com.blackzion.Dao;

import java.util.List;
import com.blackzion.models.PaquetesVacacionales;

public interface PacksVacacionalesDao {
	public List <PaquetesVacacionales> obtenerPacks();
	
	public void savePackVacacionales (PaquetesVacacionales pac);
	public PaquetesVacacionales findByIdPackVacacional (int idPackVacacional);
	
	public void updatePaquetesVacacionales (PaquetesVacacionales pac);
	public void deletePaquetesVacacionales (PaquetesVacacionales pac);
}
