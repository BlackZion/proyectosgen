package com.blackzion.Dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.blackzion.models.Servicios;

@SuppressWarnings("unchecked")
@Transactional
@Repository
public class ServiciosImpl implements ServiciosDao{

	@Autowired
	private SessionFactory sessionFactory;
	
	public Session getSession() {
		return sessionFactory.getCurrentSession();
	}

	@Override
	public List<Servicios> obtenerServicios() {
		Query<Servicios> query = getSession().createQuery("from Servicios");
			return query.list();
	}

	@Override
	public void saveServicios(Servicios ser) {
			getSession().save(ser);
	}

	@SuppressWarnings("deprecation")
	@Override
	public Servicios findByIdServicios(int id_servicios) {
		Criteria crit = getSession().createCriteria(Servicios.class);
		crit.add(Restrictions.eq("idServicios", id_servicios));
		return (Servicios) crit.uniqueResult();
	}

	@Override
	public void updateServicios(Servicios seru) {
			getSession().update(seru);
	}

	@Override
	public void deleteServicios(Servicios serd) {
			getSession().delete(serd);
	}

}
