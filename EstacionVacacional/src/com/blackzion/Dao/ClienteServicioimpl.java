package com.blackzion.Dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.blackzion.models.ClienteServicios;


@Transactional
@Repository
public class ClienteServicioimpl implements ClienteservicioDao{
  
	@Autowired
	private SessionFactory sessionFactory;
	
	public Session getSession() {
		return sessionFactory.getCurrentSession();
	}
	
	
	@Override
	public List<ClienteServicios> obtenerClienteservicio() {
		@SuppressWarnings("unchecked")
		Query<ClienteServicios> query = getSession().createQuery("from cliente_servicios");
		return  query.list();
	}

	@Override
	public void saveClienteservicio(ClienteServicios clser) {
		getSession().save(clser);
		
	}

	@SuppressWarnings("deprecation")
	@Override
	public ClienteServicios findByidClienteservicio(int idClienteServicio) {
		Criteria crit = getSession().createCriteria(ClienteServicios.class);
		crit.add(Restrictions.eq("idClienteservicio", idClienteServicio));
		return (ClienteServicios) crit.uniqueResult();
		
	}

	@Override
	public void updateClienteservicio(ClienteServicios clser) {
		getSession().update(clser);
		
	}

	@Override
	public void deleteClienteservicio(ClienteServicios clser) {
		getSession().delete(clser);
		
	}

	
}
