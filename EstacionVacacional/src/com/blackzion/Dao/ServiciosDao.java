package com.blackzion.Dao;

import java.util.List;

import com.blackzion.models.Servicios;

public interface ServiciosDao {
	public List<Servicios> obtenerServicios();
	
	public void saveServicios (Servicios ser);
	public Servicios findByIdServicios(int idServicios);
	
	public void updateServicios (Servicios ser);
	public void deleteServicios (Servicios ser);
}
