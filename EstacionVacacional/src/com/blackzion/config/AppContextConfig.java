package com.blackzion.config;

import java.util.Properties;

import javax.sql.DataSource;

import org.apache.commons.dbcp2.BasicDataSource;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBuilder;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.blackzion.Dao.ClienteServicioimpl;
import com.blackzion.Dao.ClientesDao;
import com.blackzion.Dao.ClientesImpl;
import com.blackzion.Dao.ClienteservicioDao;
import com.blackzion.Dao.FacturaDao;
import com.blackzion.Dao.FacturaImpl;
import com.blackzion.Dao.PacksVacacionalesDao;
import com.blackzion.Dao.PacksVacacionalesImpl;
import com.blackzion.Dao.TPaquetesVacacionalesDao;
import com.blackzion.Dao.TPaquetesVacacionalesImpl;
import com.blackzion.Dao.ServiciosDao;
import com.blackzion.Dao.ServiciosImpl;
import com.blackzion.models.Factura;
import com.blackzion.models.Servicios;


@Configuration
@ComponentScan(basePackages = "com.blackzion")
@EnableTransactionManagement(proxyTargetClass = true)
public class AppContextConfig {

	
	@Bean(name = "dataSource")
	public DataSource getDataSource() {
		BasicDataSource dataSource = new BasicDataSource();
		dataSource.setDriverClassName("com.mysql.jdbc.Driver");
		dataSource.setUrl("jdbc:mysql://localhost:3306/Ejercicio_Practico?useSSL=false");
		dataSource.setUsername("root");
		dataSource.setPassword("root");		
		return dataSource;
	}
	
	private Properties gethibernateProperties() {
		Properties properties = new Properties();
		properties.put("hibernate.show_sql", "true");
		properties.put("hibernate.dialect", "org.hibernate.dialect.MySQLDialect");
		return properties;
	}
	
	@Autowired
	@Bean(name = "sessionFactory")
	public SessionFactory getsessionFactory(DataSource dataSource) {
		LocalSessionFactoryBuilder sessionBuilder = new LocalSessionFactoryBuilder(dataSource);
		sessionBuilder.scanPackages("com.blackzion.models");
		sessionBuilder.addProperties(gethibernateProperties());
		
		return sessionBuilder.buildSessionFactory();
	}
	
	@Autowired
	@Bean(name = "transactionManager")
	public HibernateTransactionManager getTransactionManger(SessionFactory sessionFactory) {
		HibernateTransactionManager transactionManager = new HibernateTransactionManager(sessionFactory);
		return transactionManager;
	}
	
	@Bean(name = "facturaimpl")
	public FacturaDao facturaDao(SessionFactory sessionFactory) {
		return new FacturaImpl();
	}
	
	@Bean(name = "cliente")
	public ClientesDao clienteDao(SessionFactory sessionFactory) {
		return new ClientesImpl();
	}
	
	@Bean(name = "clienteServicios")
	public ClienteservicioDao clienteserviceDao(SessionFactory sessionFactory) {
		return new ClienteServicioimpl();
	}
	
	
	@Bean(name = "pkvacacional")
	public PacksVacacionalesDao packvacacionalesDao(SessionFactory sessionFactory) {
		return new PacksVacacionalesImpl();
		
	}
	
	@Bean(name = "tpaquetesVacacionales")
	public TPaquetesVacacionalesDao TpaquetesDao(SessionFactory sessionFactory) {
		return new TPaquetesVacacionalesImpl();
	}
	@Bean(name = "servicioDao")
	public ServiciosDao servicioDao(SessionFactory sessionFactory) {
		return new ServiciosImpl();
	}
}
