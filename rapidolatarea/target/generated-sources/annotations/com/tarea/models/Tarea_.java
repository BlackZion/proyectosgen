package com.tarea.models;

import com.tarea.models.Usuario;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.0.v20130507-rNA", date="2019-11-15T16:47:19")
@StaticMetamodel(Tarea.class)
public class Tarea_ { 

    public static volatile SingularAttribute<Tarea, String> descripcion;
    public static volatile SingularAttribute<Tarea, Integer> estado;
    public static volatile SingularAttribute<Tarea, Integer> idTarea;
    public static volatile SingularAttribute<Tarea, Usuario> idUsuario;
    public static volatile SingularAttribute<Tarea, String> titulo;
    public static volatile SingularAttribute<Tarea, Date> inicio;
    public static volatile SingularAttribute<Tarea, Date> fin;
    public static volatile SingularAttribute<Tarea, Integer> prioridad;

}