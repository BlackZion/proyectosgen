/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tarea.impl;

import com.tarea.models.Usuario;
import com.tarea.util.AbstractFacade;
import com.tarea.util.Dao;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author rodrigo.martinezusam
 */
public class usuarioimpl extends AbstractFacade<Usuario> implements Dao<Usuario>{

    
    private EntityManager em;
    
    public usuarioimpl() {
        super(Usuario.class);
        EntityManagerFactory emf;
        emf = Persistence.createEntityManagerFactory("agenda");
        em = emf.createEntityManager();
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;        
    }
    
}
