/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tarea.util;

import java.util.List;

/**
 *
 * @author rodrigo.martinezusam
 */
public interface Dao<T> {
    

    void create(T t);

    void edit(T t);

    void remove(T t);    

    List<T> findAll();

    int count();
    

}
