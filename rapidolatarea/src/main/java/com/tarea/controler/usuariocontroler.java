/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tarea.controler;

import com.tarea.impl.usuarioimpl;
import com.tarea.models.Usuario;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author rodrigo.martinezusam
 */
@ManagedBean(name = "UsdCtrl")
@SessionScoped

public class usuariocontroler implements Serializable {

    private usuarioimpl usuariodao;
    private Usuario usuario;
    private List<Usuario> listausu;

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public List<Usuario> getListausu() {       
        return listausu;
    }

    public void setListausu(List<Usuario> listausu) {
        this.listausu = listausu;
    }

    @PostConstruct
    public void init() {
        usuario = new Usuario();
    }

    public void crear() {
        try {
            usuariodao.create(usuario);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void listar(){
        try {
            this.listausu = usuariodao.findAll();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
