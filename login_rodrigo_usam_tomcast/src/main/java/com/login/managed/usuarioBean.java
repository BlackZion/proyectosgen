/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.login.managed;

import java.io.Serializable;
import com.login.Dao.usuarioDAO;
import java.util.List;
import com.login.entity.usuario;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@ManagedBean(name = "usuarioBean")
@SessionScoped
public class usuarioBean implements Serializable {

   
    private usuarioDAO usdao;
    private usuario user;
    private List<usuario> lista;

    public usuario getUser() {
        return user;
    }

    public void setUser(usuario user) {
        this.user = user;
    }

    public List<usuario> getLista() {
        return lista;
    }

    public void setLista(List<usuario> lista) {
        this.lista = lista;
    }

 
    @PostConstruct

    public void init() {
        user = new usuario();
        usdao = new usuarioDAO();      
    }

    public void findAll() {
        try {
            this.lista = usdao.findAll();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public void create(){
        try {
            usdao.create(user);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
