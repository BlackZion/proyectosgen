/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.usam.entity;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name = "usuario")
public class usuario implements Serializable{
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private String usuario;
    
    @Column(name = "clave")
    private String clave;
    
    @ManyToOne
    @JoinColumn(name = "id_rol")
    private rol_usuario id_rol;
    
    @ManyToOne
    @JoinColumn(name="id_sucursal")
    private sucursal id_sucursal;

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public rol_usuario getId_rol() {
        return id_rol;
    }

    public void setId_rol(rol_usuario id_rol) {
        this.id_rol = id_rol;
    }

    public sucursal getId_sucursal() {
        return id_sucursal;
    }

    public void setId_sucursal(sucursal id_sucursal) {
        this.id_sucursal = id_sucursal;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 47 * hash + Objects.hashCode(this.usuario);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final usuario other = (usuario) obj;
        if (!Objects.equals(this.usuario, other.usuario)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "usuario{" + "usuario=" + usuario + '}';
    }

}
