/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.usam.DAO;

import com.usam.entity.usuario;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import com.usam.util.AbstractFacade;
import com.usam.util.InterfaceFacade;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;



public class usuarioDAO extends AbstractFacade<usuario> implements InterfaceFacade<usuario> {
     
           
    private EntityManager em;
    
    @Override
    protected EntityManager getEntityManager() {
        return em;        
    }
    
    public usuarioDAO() {
        super(usuario.class);        
        EntityManagerFactory emf;
        emf = Persistence.createEntityManagerFactory("sec_dental");
        em = emf.createEntityManager();
    }
          
}
