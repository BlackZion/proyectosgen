/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.usam.util;

import com.usam.entity.usuario;
import java.util.List;
import javax.ejb.Local;



public interface InterfaceFacade<T> {
        
    void create(T  t);        
    
    List<T> findAll();


}
