/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.usam.util;

import com.usam.DAO.usuarioDAO;
import com.usam.entity.usuario;
import com.usam.entity.usuario_;
import java.util.List;
import javax.persistence.EntityManager;

public abstract class AbstractFacade<T> {

    private Class<T> entityClass;

    public AbstractFacade(Class<T> entityClass) {
        this.entityClass = entityClass;
    }

    protected abstract EntityManager getEntityManager();

    public void create(T entity) {
        getEntityManager().persist(entity);
    }

    public void edit(T entity) {
        getEntityManager().merge(entity);
    }

    public void remove(T entity) {
        getEntityManager().remove(getEntityManager().merge(entity));
    }    

    public List<T> findAll() {
        System.out.println("Soy este usuario:_:"+usuario_.usuario);
        javax.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(entityClass));
        System.out.println("Result List ?:"+getEntityManager().createQuery(cq).getResultList());
        return getEntityManager().createQuery(cq).getResultList();
        
    }    
}
