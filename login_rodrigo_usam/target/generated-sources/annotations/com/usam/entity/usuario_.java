package com.usam.entity;

import com.usam.entity.rol_usuario;
import com.usam.entity.sucursal;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.0.v20130507-rNA", date="2019-10-19T10:17:42")
@StaticMetamodel(usuario.class)
public class usuario_ { 

    public static volatile SingularAttribute<usuario, rol_usuario> id_rol;
    public static volatile SingularAttribute<usuario, String> clave;
    public static volatile SingularAttribute<usuario, String> usuario;
    public static volatile SingularAttribute<usuario, sucursal> id_sucursal;

}