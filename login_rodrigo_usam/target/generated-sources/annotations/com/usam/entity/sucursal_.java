package com.usam.entity;

import com.usam.entity.empresa;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.0.v20130507-rNA", date="2019-10-19T10:17:42")
@StaticMetamodel(sucursal.class)
public class sucursal_ { 

    public static volatile SingularAttribute<sucursal, String> descripcion;
    public static volatile SingularAttribute<sucursal, String> direccion;
    public static volatile SingularAttribute<sucursal, empresa> id_empresa;
    public static volatile SingularAttribute<sucursal, Integer> id;
    public static volatile SingularAttribute<sucursal, Integer> telefono;

}