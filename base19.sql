create database participante19;
use participante19;

create table if not exists cliente(
id_cliente int not null primary key auto_increment,
nombre varchar(25),
apellido varchar(25),
direccion varchar(45),
fecha_nacimiento date,
telefono varchar(9),
email varchar(45)
)engine InnoDB;

insert into cliente value(null,'Rodrigo Martin','Martinez Aleman','MI CASA','2015-05-05','22970810','PARAQUE@GMAIL.COM');


create table if not exists modo_pago (
num_pago int not null primary key auto_increment,
nombre varchar(45),
otros_detalles varchar(55)
)engine InnoDB;


create table if not exists factura(
num_factura int not null primary key auto_increment,
id_cliente int not null,
fecha date,
num_pago int not null,
constraint cliente foreign key(id_cliente) references cliente(id_cliente) on delete cascade on update cascade,
constraint pago foreign key(num_pago) references modo_pago(num_pago) on delete cascade on update cascade
)engine InnoDB;


create table if not exists categoria(
id_categoria int not null primary key auto_increment,
nombre varchar(45),
descripcion varchar(55)
)engine InnoDB;

insert into categoria value(null,'carnes','pollo asado');

create table if not exists producto(
id_producto int not null primary key auto_increment,
nombre varchar(45) not null,
precio double not null,
stock int not null,
id_categoria int not null,
constraint categoria foreign key(id_categoria) references categoria(id_categoria) on delete cascade on update cascade
)engine InnoDB;

insert into producto value(null,'pollo asado en salsa','22.5','5',1);



create table if not exists detalle(
num_detalle int not null primary key auto_increment,
id_factura int not null,
id_producto int not null,
cantidad int not null,
precio double,
constraint factura foreign key(id_factura) references factura(num_factura) on delete cascade on update cascade,
constraint producto foreign key(id_producto) references producto(id_producto) on delete cascade on update cascade
)engine InnoDB;