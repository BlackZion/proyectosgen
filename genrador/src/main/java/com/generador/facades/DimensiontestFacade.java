/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.generador.facades;

import com.tarea.genrador.Dimensiontest;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author rodrigo.martinezusam
 */
@Stateless
public class DimensiontestFacade extends AbstractFacade<Dimensiontest> implements DimensiontestFacadeLocal {

    @PersistenceContext(unitName = "persistence")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public DimensiontestFacade() {
        super(Dimensiontest.class);
    }
    
}
