/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.generador.facades;

import com.tarea.genrador.Dimensiontest;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author rodrigo.martinezusam
 */
@Local
public interface DimensiontestFacadeLocal {

    void create(Dimensiontest dimensiontest);

    void edit(Dimensiontest dimensiontest);

    void remove(Dimensiontest dimensiontest);

    Dimensiontest find(Object id);

    List<Dimensiontest> findAll();

    List<Dimensiontest> findRange(int[] range);

    int count();
    
}
