/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.generador.facades;

import com.tarea.genrador.Nivelesc;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author rodrigo.martinezusam
 */
@Local
public interface NivelescFacadeLocal {

    void create(Nivelesc nivelesc);

    void edit(Nivelesc nivelesc);

    void remove(Nivelesc nivelesc);

    Nivelesc find(Object id);

    List<Nivelesc> findAll();

    List<Nivelesc> findRange(int[] range);

    int count();
    
}
