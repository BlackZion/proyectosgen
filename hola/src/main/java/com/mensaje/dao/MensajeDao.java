/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main.java.com.mensaje.dao;

import main.java.com.mensaje.models.Mensaje;
import main.java.com.mensaje.util.Dao;
import main.java.com.mensaje.util.Enlace;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author rodrigo.martinezusam|
 */
@Repository
public class MensajeDao implements Dao<Mensaje> {

    @Autowired
    Connection getConn;
    private PreparedStatement p;
    private String[] ar = {"Insert into mensaje(mensaje) value(?)",
        "Select * from mensaje order by id"};

    private Mensaje m;
    private List<Mensaje> men;

    public MensajeDao() {
    }

    
    @Override
    public void create(Mensaje t) {
        try {
            System.out.println("Entre al implementacion");
            //Porqué no se inicia la transacción con el Enlace//
            p = Enlace.conect().prepareStatement(ar[0]);
            p.setString(1, t.getMsj());
            p.executeUpdate();
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(
                    null,
                    new FacesMessage(FacesMessage.SEVERITY_WARN,
                            "Error al Crear Mensaje",
                            "En el Metodo Dao"));
        } finally {
            Enlace.remove();
        }
    }

    @Override
    public List<Mensaje> read() {
        try {
            p = Enlace.conect().prepareStatement(ar[1]);
             //p = getConex.prepareStatement(ar[1]);                
            ResultSet rs = p.executeQuery();
            men = new ArrayList<Mensaje>();
            while (rs.next()) {

                m = new Mensaje();
                m.setId(rs.getLong("id"));                
                m.setMsj(rs.getString("mensaje"));
                men.add(m);

            }

            return men;
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Error en Cliente: Al leer");
            return null;
        } finally {
            Enlace.remove();
        }
    }

}
