
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main.java.com.mensaje.util;

import java.util.List;

/**
 *
 * @author rodrigo.martinezusam
 */
public interface Dao<T> {

    public void create(T t);

    public List<T> read();
}
