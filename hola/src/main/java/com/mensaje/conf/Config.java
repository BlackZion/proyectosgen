/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main.java.com.mensaje.conf;

import main.java.com.mensaje.dao.MensajeDao;
import main.java.com.mensaje.util.Dao;
import java.sql.Connection;

import java.sql.DriverManager;
import java.sql.SQLException;
import org.springframework.context.annotation.Bean;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author rodrigo.martinezusam
 */

@Configuration
@ComponentScan(basePackages = {"main.java.com.mensaje"})
public class Config {

    @Bean
    public Dao daoMensaje() {
        return new MensajeDao();
    }

    @Bean
    public Connection getConn() {
        Connection conn;
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/mensajes?useSSL=false","root","root");
        } catch (SQLException e) {
            conn = null;
        } catch (ClassNotFoundException x) {
            conn = null;
        }
        return conn;
    }
}
