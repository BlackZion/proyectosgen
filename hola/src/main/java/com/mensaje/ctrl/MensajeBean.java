/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main.java.com.mensaje.ctrl;

import main.java.com.mensaje.models.Mensaje;
import  main.java.com.mensaje.util.Dao;
import java.io.Serializable;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.FacesContext;
import org.springframework.stereotype.Component;

/**
 *
 * @author rodrigo.martinezusam
 */
@Component
@RequestScoped
@ManagedBean(name = "MsjCtrl")
public class MensajeBean implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = -7997901060630276705L;
	@ManagedProperty("#{daoMensaje}")

    private Dao<Mensaje> daoMensaje;    
    private Mensaje mensaje;
    

    public Mensaje getMensaje() {
        return mensaje;
    }

    public void setMensaje(Mensaje mensaje) {
        this.mensaje = mensaje;
    }

    public Dao<Mensaje> getDaoMensaje() {
        return daoMensaje;
    }

    public void setDaoMensaje(Dao<Mensaje> daoMensaje) {
        this.daoMensaje = daoMensaje;
    }

    public MensajeBean() {

    }

    public void createMensaje() {        
        try {            
            System.out.println("Intentando metodo");
            this.daoMensaje.create(mensaje);
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_FATAL,
                            "Error",
                            "En el controlador"));
        }
    }
    public List<Mensaje> listar() {
        try {
            List<Mensaje> ls = daoMensaje.read();
            return ls;
        } catch (Exception x) {
            return null;
        }

    }
}
