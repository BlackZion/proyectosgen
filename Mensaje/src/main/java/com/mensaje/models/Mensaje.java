/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mensaje.models;

/**
 *
 * @author rodrigo.martinezusam
 */
public class Mensaje {
    
    private long id;
    private String msj;

    public Mensaje() {    
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getMsj() {
        return msj;
    }

    public void setMsj(String msj) {
        this.msj = msj;
    }
    
    
    
    
}
