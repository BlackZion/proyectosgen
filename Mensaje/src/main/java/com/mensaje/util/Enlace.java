/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mensaje.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author rodrigo.martinezusam
 */
public class Enlace {

    private static Connection conn;

    public static Connection conect() {
        try {
        
            Class.forName("com.mysql.jdbc.Driver");
            
            conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/mensajes?useSSL=false", "root", "root");
            
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
            conn = null;
        }
        return conn;
    }

    public static void remove() {
        try {
            if (conn != null) {
               if(!conn.isClosed()){
               conn.close();
               }
            }
        } catch (SQLException e) {
        }
    }

}
