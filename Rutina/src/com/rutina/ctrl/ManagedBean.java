package com.rutina.ctrl;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.SessionScope;
import org.springframework.beans.factory.annotation.Qualifier;

import com.rutina.dao.Actividadimpl;
import com.rutina.dao.Estadoimpl;
import com.rutina.dao.Fasesimpl;
import com.rutina.dao.FechaEspimpl;
import com.rutina.dao.Personaimpl;
import com.rutina.dao.TipoUsuarioimpl;
import com.rutina.dao.Usuarioimpl;
import com.rutina.models.Actividad;
import com.rutina.models.Estado;
import com.rutina.models.Fases;
import com.rutina.models.FechasEsp;
import com.rutina.models.Persona;
import com.rutina.models.TipoUsuario;
import com.rutina.models.Usuario;

@Component
@SessionScope
@javax.faces.bean.ManagedBean(name = "ActBean")
public class ManagedBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
     
	@Autowired
	@Qualifier("persona")
	private Personaimpl PersonaDao;
    private List<Persona> listpersona;
    private Persona persona;
    
    
    @Autowired
    @Qualifier("actividad")
    private Actividadimpl actividadDao;
    private List<Actividad> listActividad;
    private Actividad actividad;
    
    @Autowired
    @Qualifier("Estado")
    private Estadoimpl estadoDao;
    private List<Estado> listestado;
    private Estado estado;
    
    
    @Autowired
    @Qualifier("fases")
    private Fasesimpl fasesDao;
    private List<Fases> listFases;
    private Fases fases;
    
    
    @Autowired
    @Qualifier("fechaEsp")
    private FechaEspimpl fechaDao;
    private List<FechasEsp> listFechaesp;
    private FechasEsp fechasEsp;
    
    
    @Autowired
    @Qualifier("tipousuario")
    private TipoUsuarioimpl tipoUsuarioDao;
    private List<TipoUsuario> listTipousuario;
    private TipoUsuario tipoUsuario;
    
    
    @Autowired
    @Qualifier("usuario")
    private Usuarioimpl usuarioDao;
    private List<Usuario> listUsuario;
    private Usuario usuario;
    
   
    

	public List<Persona> getListpersona() {
		return listpersona;
	}




	public void setListpersona(List<Persona> listpersona) {
		this.listpersona = listpersona;
	}




	public Persona getPersona() {
		return persona;
	}




	public void setPersona(Persona persona) {
		this.persona = persona;
	}




	public List<Actividad> getListActividad() {
		return listActividad;
	}




	public void setListActividad(List<Actividad> listActividad) {
		this.listActividad = listActividad;
	}




	public Actividad getActividad() {
		return actividad;
	}




	public void setActividad(Actividad actividad) {
		this.actividad = actividad;
	}




	public List<Estado> getListestado() {
		return listestado;
	}




	public void setListestado(List<Estado> listestado) {
		this.listestado = listestado;
	}




	public Estado getEstado() {
		return estado;
	}




	public void setEstado(Estado estado) {
		this.estado = estado;
	}




	public List<Fases> getListFases() {
		return listFases;
	}




	public void setListFases(List<Fases> listFases) {
		this.listFases = listFases;
	}




	public Fases getFases() {
		return fases;
	}




	public void setFases(Fases fases) {
		this.fases = fases;
	}




	public List<FechasEsp> getListFechaesp() {
		return listFechaesp;
	}




	public void setListFechaesp(List<FechasEsp> listFechaesp) {
		this.listFechaesp = listFechaesp;
	}




	public FechasEsp getFechasEsp() {
		return fechasEsp;
	}




	public void setFechasEsp(FechasEsp fechasEsp) {
		this.fechasEsp = fechasEsp;
	}




	public List<TipoUsuario> getListTipousuario() {
		return listTipousuario;
	}




	public void setListTipousuario(List<TipoUsuario> listTipousuario) {
		this.listTipousuario = listTipousuario;
	}




	public TipoUsuario getTipoUsuario() {
		return tipoUsuario;
	}




	public void setTipoUsuario(TipoUsuario tipoUsuario) {
		this.tipoUsuario = tipoUsuario;
	}




	public List<Usuario> getListUsuario() {
		return listUsuario;
	}




	public void setListUsuario(List<Usuario> listUsuario) {
		this.listUsuario = listUsuario;
	}




	public Usuario getUsuario() {
		return usuario;
	}




	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}




	@PostConstruct
	public void init() {
		persona = new Persona();	
		actividad = new Actividad();
		estado = new Estado();
		fases = new Fases();
		fechasEsp = new FechasEsp();
		persona = new Persona();
		tipoUsuario = new TipoUsuario();
		usuario = new Usuario();
		
	}
	
	
	public void createActividad() {
		try {
			actividad.setIdUsuario(usuario);
			actividad.setIdEstado(estado);
			actividad.setIdFase(fases);
			
		} catch (Exception e) {
		 e.printStackTrace();
		}
		
	}
	
	public void ingreso() {
		try {
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		
	}
	
	
		
    
	
}
