package com.rutina.ctrl;

import java.io.Serializable;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.Temporal;
import java.util.Calendar;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.primefaces.event.CloseEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.SessionScope;

import com.rutina.dao.EspDao;
import com.rutina.dao.GenericDAO;
import com.rutina.dao.Usuarioimpl;
import com.rutina.models.Actividad;
import com.rutina.models.Estado;
import com.rutina.models.Fases;
import com.rutina.models.FechasEsp;
import com.rutina.models.Persona;
import com.rutina.models.TipoUsuario;
import com.rutina.models.Usuario;
@SessionScope
@Component
@ManagedBean(name = "calendar")
public class CalendarioBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Autowired
	@Qualifier("actividad")
	private GenericDAO<Actividad> ActividadDao;
	private Actividad actividad;
	private List<Actividad> listact;

	@Autowired
	@Qualifier("fases")
	private GenericDAO<Fases> FasesDao;
	private Fases fases;
	private List<Fases> lisfases;

	@Autowired
	@Qualifier("fechaEsp")
	private GenericDAO<FechasEsp> FechaEspDao;
	private FechasEsp fechasEsp;
	private List<FechasEsp> listfecha;

	@Autowired
	@Qualifier("tipousuario")
	private GenericDAO<TipoUsuario> tipousuarioDao;
	private TipoUsuario tipoUsuario;
	private List<TipoUsuario> listusuariostipo;

	@Autowired
	@Qualifier("persona")
	private GenericDAO<Persona> PersonaDao;
	private Persona persona;
	private List<Persona> listpersona;

	@Autowired
	@Qualifier("usuario")
	private EspDao UsuarioDao;
	private Usuario usuario;
	private List<Usuario> listusuario;
	
	
	@Autowired
	private Usuarioimpl Userimpl;

	@Autowired
	@Qualifier("Estado")
	private GenericDAO<Estado> EstadoDao;
	private Estado estado;
	private List<Estado> listestado;

	public Actividad getActividad() {
		return actividad;
	}

	public void setActividad(Actividad actividad) {
		this.actividad = actividad;
	}

	public List<Actividad> getListact() {
		return listact;
	}

	public void setListact(List<Actividad> listact) {
		this.listact = listact;
	}

	public Fases getFases() {
		return fases;
	}

	public void setFases(Fases fases) {
		this.fases = fases;
	}

	public List<Fases> getLisfases() {
		return lisfases;
	}

	public void setLisfases(List<Fases> lisfases) {
		this.lisfases = lisfases;
	}

	public FechasEsp getFechasEsp() {
		return fechasEsp;
	}

	public void setFechasEsp(FechasEsp fechasEsp) {
		this.fechasEsp = fechasEsp;
	}

	public List<FechasEsp> getListfecha() {
		return listfecha;
	}

	public void setListfecha(List<FechasEsp> listfecha) {
		this.listfecha = listfecha;
	}

	public TipoUsuario getTipoUsuario() {
		return tipoUsuario;
	}

	public void setTipoUsuario(TipoUsuario tipoUsuario) {
		this.tipoUsuario = tipoUsuario;
	}

	public List<TipoUsuario> getListusuariostipo() {
		this.listusuario = this.UsuarioDao.findAll();
		return listusuariostipo;
	}

	public void setListusuariostipo(List<TipoUsuario> listusuariostipo) {
		this.listusuariostipo = listusuariostipo;
	}

	public Persona getPersona() {
		return persona;
	}

	public void setPersona(Persona persona) {
		this.persona = persona;
	}

	public List<Persona> getListpersona() {
		this.listpersona = PersonaDao.findAll();
		return listpersona;
	}

	public void setListpersona(List<Persona> listpersona) {
		this.listpersona = listpersona;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public List<Usuario> getListusuario() {
		return listusuario;
	}

	public void setListusuario(List<Usuario> listusuario) {
		this.listusuario = listusuario;
	}

	public boolean isShowTime() {
		return showTime;
	}

	public void setShowTime(boolean showTime) {
		this.showTime = showTime;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Estado getEstado() {
		return estado;
	}

	public void setEstado(Estado estado) {
		this.estado = estado;
	}

	public List<Estado> getListestado() {
		return listestado;
	}

	public void setListestado(List<Estado> listestado) {
		this.listestado = listestado;
	}

	@PostConstruct
	public void init() {
		usuario = new Usuario();
		persona = new Persona();
		actividad = new Actividad();
		fases = new Fases();
		estado = new Estado();
	}

	public void viewlit() {
		try {
			this.listusuario = Userimpl.findAll();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private Date time1;
	private Date time2;
	private LocalTime time3;
	private LocalTime time4;
	private LocalTime time5;
	private Date time6;
	private boolean showTime = false;
	private String locale = "en_US";

	public void TimePickerController() {
		final Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.AM_PM, Calendar.AM);
		calendar.set(Calendar.HOUR, 8);
		calendar.set(Calendar.MINUTE, 15);
		time1 = (Date) calendar.getTime();

		calendar.set(Calendar.HOUR, 11);
		calendar.set(Calendar.MINUTE, 40);
		time2 = (Date) calendar.getTime();

		time3 = LocalTime.now();

		time5 = LocalTime.now();

		time6 = new Date(0);
	}

	public Date getTime1() {
		return time1;
	}

	public void setTime1(final Date time1) {
		this.time1 = time1;
	}

	public Date getTime2() {
		return time2;
	}

	public void setTime2(final Date time2) {
		this.time2 = time2;
	}

	public LocalTime getTime3() {
		return time3;
	}

	public void setTime3(final LocalTime time3) {
		this.time3 = time3;
	}

	public LocalTime getTime4() {
		return time4;
	}

	public void setTime4(final LocalTime time4) {
		this.time4 = time4;
	}

	public LocalTime getTime5() {
		return time5;
	}

	public void setTime5(final LocalTime time5) {
		this.time5 = time5;
	}

	public Date getTime6() {
		return time6;
	}

	public void setTime6(final Date time6) {
		this.time6 = time6;
	}

	public String getLocale() {
		return locale;
	}

	public void setLocale(final String locale) {
		this.locale = locale;
	}

	public String getFormattedTime1() {
		return getFormattedTime(time1, "HH:mm");
	}

	public String getFormattedTime2() {
		return getFormattedTime(time2, "HH:mm");
	}

	public String getFormattedTime3() {
		return getFormattedTime(time3, "HH:mm");
	}

	public String getFormattedTime4() {
		return getFormattedTime(time4, "hh-mm a");
	}

	public String getFormattedTime5() {
		return getFormattedTime(time5, "HH:mm");
	}

	public String getFormattedTime6() {
		return getFormattedTime(time6, "HH:mm");
	}

	public void showTime(final ActionEvent ae) {
		showTime = true;
	}

	public void saveAct() {

		try {
			actividad.setIdUsuario(usuario);
			actividad.setIdFase(fases);
			actividad.setIdEstado(estado);
			ActividadDao.save(actividad);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public boolean isShowTimeDialog() {
		if (showTime) {
			showTime = false;

			return true;
		}

		return false;
	}

	public void closeListener(final CloseEvent closeEvent) {
		final FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Close fired",
				"Component id: " + closeEvent.getComponent().getId());
		FacesContext.getCurrentInstance().addMessage(null, msg);
	}

	private String getFormattedTime(final Date time12, final String format) {
		if (time12 == null) {
			return null;
		}

		final SimpleDateFormat formatter = new SimpleDateFormat(format);
		return formatter.format(time12);
	}

	private String getFormattedTime(final Temporal time, final String format) {
		if (time == null) {
			return null;
		}

		final DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
		return formatter.format(time);
	}

}
