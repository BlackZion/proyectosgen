package com.rutina.ctrl;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.SessionScope;

import com.rutina.dao.EspDao;
import com.rutina.dao.TipoUsuarioimpl;
import com.rutina.dao.Usuarioimpl;
import com.rutina.models.TipoUsuario;
import com.rutina.models.Usuario;

@Component
@ManagedBean(name = "CtrlUser")
@SessionScope
public class Ingreso implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Autowired
	@Qualifier("tipousuario")
	private TipoUsuarioimpl tipoUsuarioDao;
	private  EspDao DaoEsp;
	private List<TipoUsuario> listTipousuario;
	private TipoUsuario tipoUsuario;

	@Autowired
	@Qualifier("usuario")
	private Usuarioimpl usuarioDao;
	private List<Usuario> listUsuario;
	private Usuario usuario;
	private Usuario user;

	public List<TipoUsuario> getListTipousuario() {
		return listTipousuario;
	}

	public void setListTipousuario(List<TipoUsuario> listTipousuario) {
		this.listTipousuario = listTipousuario;
	}

	public TipoUsuario getTipoUsuario() {
		return tipoUsuario;
	}

	public void setTipoUsuario(TipoUsuario tipoUsuario) {
		this.tipoUsuario = tipoUsuario;
	}

	public List<Usuario> getListUsuario() {
		return listUsuario;
	}

	public void setListUsuario(List<Usuario> listUsuario) {
		this.listUsuario = listUsuario;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Usuario getUser() {
		return user;
	}

	public void setUser(Usuario user) {
		this.user = user;
	}

	@PostConstruct
	public void init() {
		usuario = new Usuario();
		tipoUsuario = new TipoUsuario();
	}

	public String SessionLog() {
		String ruta = null;
		try {
			user = DaoEsp.login(usuario);
			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("user", user);
			ruta = "main.xhtml?faces-redirect=true";	
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ruta;

	}

}
