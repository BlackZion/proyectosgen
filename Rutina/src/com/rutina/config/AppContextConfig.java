package com.rutina.config;

import java.util.Properties;

import javax.sql.DataSource;

import org.apache.commons.dbcp2.BasicDataSource;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBuilder;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.view.InternalResourceViewResolver;


import com.rutina.dao.Actividadimpl;
import com.rutina.dao.EspDao;
import com.rutina.dao.Estadoimpl;
import com.rutina.dao.Fasesimpl;
import com.rutina.dao.FechaEspimpl;
import com.rutina.dao.GenericDAO;
import com.rutina.dao.Personaimpl;
import com.rutina.dao.TipoUsuarioimpl;
import com.rutina.dao.Usuarioimpl;
import com.rutina.models.Actividad;
import com.rutina.models.Estado;
import com.rutina.models.Fases;
import com.rutina.models.FechasEsp;
import com.rutina.models.Persona;
import com.rutina.models.TipoUsuario;
import com.rutina.models.Usuario;

@Configuration
@ComponentScan(basePackages = "com.rutina")
@EnableTransactionManagement(proxyTargetClass = true)
public class AppContextConfig {
	

	@Bean
	public SessionFactory getConex() {
		return HibernateUtil.getSessionFactory();
	}

	@Bean
	public Personaimpl persona() {
		return new Personaimpl();
	}

	
}

