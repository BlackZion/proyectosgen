package com.rutina.dao;

import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;

import com.rutina.models.Usuario;
import com.rutina.config.HibernateUtil;

@Transactional
@Repository(value ="usuario")
public class Usuarioimpl extends AbstractFacade<Usuario> implements EspDao{

	
	private Session session = HibernateUtil.getSessionFactory().openSession();

	
	public Usuarioimpl() {
		super(Usuario.class);			
	}


	@Override
	public Usuario login(Usuario usuario) {
		
		Transaction t = session.beginTransaction();
		try {
			CriteriaBuilder cb = session.getCriteriaBuilder();
			CriteriaQuery<Usuario> cq = cb.createQuery(Usuario.class);
			Root<Usuario> root = cq.from(Usuario.class);
			cq.where(
					cb.equal(root.get("NombreUs"), usuario.getNombreUs()),
					cb.equal(root.get("Password"), usuario.getPassword())
					);		
			 Query q = session.createQuery(cq);
			
			usuario = (Usuario) q.getResultList();
			if(usuario == null) {
				usuario = null;				
			}else {
				return usuario;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		session.flush();
		t.commit();		
		return usuario;
	}

}
