package com.rutina.dao;

import org.hibernate.SessionFactory;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.rutina.models.TipoUsuario;

@Component
@Repository(value ="tipousuario")
public class TipoUsuarioimpl extends AbstractFacade<TipoUsuario> implements GenericDAO<TipoUsuario>{

	private SessionFactory sessionFactory;
	
	public TipoUsuarioimpl() {
		super(TipoUsuario.class);
		// TODO Auto-generated constructor stub
	}

}
