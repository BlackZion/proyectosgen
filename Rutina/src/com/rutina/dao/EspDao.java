package com.rutina.dao;

import com.rutina.models.Usuario;

public interface EspDao extends GenericDAO<Usuario>{

	Usuario login(Usuario usuario);

	
}
