package com.rutina.dao;

import java.util.List;

import org.springframework.stereotype.Component;


public interface GenericDAO<T> {

   public void save(T t);

    public void delete(T t);

    public void update(T t);

    public T readById(int id);

    public List<T> findAll();
	
}
