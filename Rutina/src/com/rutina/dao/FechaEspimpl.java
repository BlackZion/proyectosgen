package com.rutina.dao;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.rutina.models.FechasEsp;

@Component
@Repository(value ="fechaEsp")
public class FechaEspimpl extends AbstractFacade<FechasEsp> implements GenericDAO<FechasEsp>{

	
	public FechaEspimpl() {
		super(FechasEsp.class);

	}

	
}
