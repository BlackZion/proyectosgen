package com.rutina.dao;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.rutina.models.Estado;

@Component
@Repository(value ="Estado")
public class Estadoimpl extends AbstractFacade<Estado> implements GenericDAO<Estado>{


	
	public Estadoimpl() {
		super(Estado.class);
	}

}
