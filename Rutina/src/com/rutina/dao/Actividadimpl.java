package com.rutina.dao;



import java.util.List;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.rutina.models.Actividad;

@Component
@Repository(value ="actividad")
public class Actividadimpl extends AbstractFacade<Actividad> implements GenericDAO<Actividad>{

	public Actividadimpl() {
		super(Actividad.class);		
	}

		



}
