/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.login.Dao;

import com.login.entity.empresa;
import javax.persistence.EntityManager;
import com.usam.util.AbstractFacade;
import com.usam.util.InterfaceFacade;
import javax.persistence.EntityManagerFactory;
import javax.persistence.*;

public class empresaDAO extends AbstractFacade<empresa> implements InterfaceFacade<empresa> {

    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public empresaDAO() {
        super(empresa.class);
        EntityManagerFactory emf;
        emf = Persistence.createEntityManagerFactory("sec_dental");
        em = emf.createEntityManager();
    }
}
