/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.login.managed;

import com.login.Dao.empresaDAO;
import java.io.Serializable;
import com.login.entity.empresa;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@ManagedBean(name = "empresaBean")
@SessionScoped
public class empresaBean implements Serializable {

    private empresaDAO emdao;
    private empresa empresa;
    private List<empresa> lista;

    public empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(empresa empresa) {
        this.empresa = empresa;
    }

    public List<empresa> getLista() {
        return lista;
    }

    public void setLista(List<empresa> lista) {
        this.lista = lista;
    }

    @PostConstruct

    public void init() {
        empresa = new empresa();
        emdao = new empresaDAO();
    }

    public void findAll() {
        try {
            this.lista = emdao.findAll();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void save() {
        try {
            emdao.create(empresa);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
