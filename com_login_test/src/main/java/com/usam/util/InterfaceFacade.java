/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.usam.util;

import java.util.List;


public interface InterfaceFacade<T> {
        
    void create(T  t);        
    
    List<T> findAll();

    void edit(T t);
    
    void remove(T t);
}
