package com.tarea.models;

import com.tarea.models.Dimensiontest;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.0.v20130507-rNA", date="2019-11-16T11:47:29")
@StaticMetamodel(Pregunta.class)
public class Pregunta_ { 

    public static volatile SingularAttribute<Pregunta, Integer> id;
    public static volatile SingularAttribute<Pregunta, Dimensiontest> idTitulo;
    public static volatile SingularAttribute<Pregunta, String> pregunta;

}