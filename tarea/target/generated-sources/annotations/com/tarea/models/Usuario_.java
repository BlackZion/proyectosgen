package com.tarea.models;

import com.tarea.models.Departamento;
import com.tarea.models.Dimensiontest;
import com.tarea.models.Nivelesc;
import com.tarea.models.Sexo;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.0.v20130507-rNA", date="2019-11-16T11:47:29")
@StaticMetamodel(Usuario.class)
public class Usuario_ { 

    public static volatile SingularAttribute<Usuario, Departamento> idDepartamento;
    public static volatile SingularAttribute<Usuario, Sexo> idSexo;
    public static volatile SingularAttribute<Usuario, Integer> id;
    public static volatile SingularAttribute<Usuario, Dimensiontest> idDimensiontest;
    public static volatile SingularAttribute<Usuario, Nivelesc> idNivelesc;
    public static volatile SingularAttribute<Usuario, String> nombre;
    public static volatile SingularAttribute<Usuario, Date> edad;

}