package com.tarea.models;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.0.v20130507-rNA", date="2019-11-16T11:47:29")
@StaticMetamodel(Dimensiontest.class)
public class Dimensiontest_ { 

    public static volatile SingularAttribute<Dimensiontest, Date> fecha;
    public static volatile SingularAttribute<Dimensiontest, String> titulo;
    public static volatile SingularAttribute<Dimensiontest, String> departamento;
    public static volatile SingularAttribute<Dimensiontest, Integer> id;
    public static volatile SingularAttribute<Dimensiontest, Integer> nivel;

}