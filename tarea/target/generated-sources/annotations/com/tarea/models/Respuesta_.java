package com.tarea.models;

import com.tarea.models.Pregunta;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.0.v20130507-rNA", date="2019-11-16T11:47:29")
@StaticMetamodel(Respuesta.class)
public class Respuesta_ { 

    public static volatile SingularAttribute<Respuesta, String> resp;
    public static volatile SingularAttribute<Respuesta, Integer> id;
    public static volatile SingularAttribute<Respuesta, Pregunta> idPregunta;

}