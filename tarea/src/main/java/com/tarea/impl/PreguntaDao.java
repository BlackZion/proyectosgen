/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tarea.impl;

import com.tarea.models.Pregunta;
import com.tarea.models.Respuesta;
import com.tarea.util.AbstractFacade;
import com.tarea.util.Interfaz;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author rodrigo.martinezusam
 */
public class PreguntaDao extends AbstractFacade<Pregunta> implements Interfaz<Pregunta> {

    private EntityManager em;

    public PreguntaDao() {
        super(Pregunta.class);
        EntityManagerFactory emf;
        emf = Persistence.createEntityManagerFactory("persistencia");
        em = emf.createEntityManager();
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
}
