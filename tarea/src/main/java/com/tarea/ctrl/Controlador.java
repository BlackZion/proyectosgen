/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tarea.ctrl;

import com.tarea.impl.*;
import com.tarea.models.*;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author rodrigo.martinezusam
 */

@ManagedBean(name = "Ctrl")
@SessionScoped
public class Controlador implements Serializable{
    
    private Usuario usuario;
    private Sexo sexo;
    private Respuesta respuesta;
    private Pregunta pregunta;
    private Nivelesc nivel;
    private Dimensiontest dimensiontest;
    private Departamento departamento;
    
    private List<Usuario> listausuList;
    private List<Sexo> listsexo;
    private List<Respuesta> Respuestalist;
    private List<Pregunta> preguntalist;
    private List<Nivelesc> Nivelesclist;
    private List<Dimensiontest> Dimensiontestlist;
    private List<Departamento> Departamentolist;

    
    private DepartamentoDao depDao;
    private NivelescDao nicDao;
    private PreguntaDao preDao;
    private RespuestaDao resDao;
    private SexoDao sexDao;
    private UsuarioDao userDao;
    private dimensionDao dimDao;
      
    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Sexo getSexo() {
        return sexo;
    }

    public void setSexo(Sexo sexo) {
        this.sexo = sexo;
    }

    public Respuesta getRespuesta() {
        return respuesta;
    }

    public void setRespuesta(Respuesta respuesta) {
        this.respuesta = respuesta;
    }

    public Pregunta getPregunta() {
        return pregunta;
    }

    public void setPregunta(Pregunta pregunta) {
        this.pregunta = pregunta;
    }

    public Nivelesc getNivel() {
        return nivel;
    }

    public void setNivel(Nivelesc nivel) {
        this.nivel = nivel;
    }

    public Dimensiontest getDimensiontest() {
        return dimensiontest;
    }

    public void setDimensiontest(Dimensiontest dimensiontest) {
        this.dimensiontest = dimensiontest;
    }

    public Departamento getDepartamento() {
        return departamento;
    }

    public void setDepartamento(Departamento departamento) {
        this.departamento = departamento;
    }

    public List<Usuario> getListausuList() {
        this.listausuList = userDao.findAll();
        return listausuList;
    }

    public void setListausuList(List<Usuario> listausuList) {
        this.listausuList = listausuList;
    }

    public List<Sexo> getListsexo() {
        this.listsexo = sexDao.findAll();
        return listsexo;
    }

    public void setListsexo(List<Sexo> listsexo) {
        this.listsexo = listsexo;
    }

    public List<Respuesta> getRespuestalist() {
        this.Respuestalist = resDao.findAll();
        return Respuestalist;
    }

    public void setRespuestalist(List<Respuesta> Respuestalist) {
        this.Respuestalist = Respuestalist;
    }

    public List<Pregunta> getPreguntalist() {
        this.preguntalist = preDao.findAll();
        return preguntalist;
    }

    public void setPreguntalist(List<Pregunta> preguntalist) {
        this.preguntalist = preguntalist;
    }

    public List<Nivelesc> getNivelesclist() {
        this.Nivelesclist = nicDao.findAll();
        return Nivelesclist;
    }

    public void setNivelesclist(List<Nivelesc> Nivelesclist) {
        this.Nivelesclist = Nivelesclist;
    }

    public List<Dimensiontest> getDimensiontestlist() {
        this.Dimensiontestlist = dimDao.findAll();
        return Dimensiontestlist;
    }

    public void setDimensiontestlist(List<Dimensiontest> Dimensiontestlist) {
        this.Dimensiontestlist = Dimensiontestlist;
    }

    public List<Departamento> getDepartamentolist() {
        this.Departamentolist = depDao.findAll();
        return Departamentolist;
    }

    public void setDepartamentolist(List<Departamento> Departamentolist) {
        this.Departamentolist = Departamentolist;
    }
    
    @PostConstruct
    public void init(){
    depDao = new DepartamentoDao();
    nicDao = new NivelescDao();
    preDao = new PreguntaDao();
    resDao = new  RespuestaDao();
    sexDao = new SexoDao();
    userDao = new UsuarioDao();
    dimDao = new dimensionDao();
    
    //Entidades
    
    usuario = new Usuario();
    sexo = new Sexo();
    departamento = new Departamento();
    dimensiontest = new Dimensiontest();
    nivel = new Nivelesc();
    pregunta = new Pregunta();
    respuesta = new Respuesta();       
    
    }
    
    
    public void llenarFormulario (){
        try {
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }   
     
}
