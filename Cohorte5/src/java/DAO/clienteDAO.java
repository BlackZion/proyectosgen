/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import conexion.Conexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;
import entidades.cliente;

/**
 *
 * @author rodrigo.martinezusam
 */
public class clienteDAO {

    Conexion conn = new Conexion();

    public clienteDAO(Conexion con) {
        this.conn = conn;
    }

    public List<cliente> findAll() {
        try {
            String query = "select * from cliente";
            PreparedStatement stm = this.conn.conectar().prepareStatement(query);
            ResultSet rs = stm.executeQuery();
            List<cliente> cliente = new LinkedList<>();
            while (rs.next()) {
                cliente com = new cliente();
                com.setId_cliente(rs.getInt("id_cliente"));
                com.setNombre(rs.getString("nombre"));
                com.setApellido(rs.getString("apellido"));
                com.setDireccion(rs.getString("direccion"));
                com.setFecha_nacimiento(rs.getDate("fecha_nacimiento"));
                com.setTelefono(rs.getString("telefono"));
                com.setEmail(rs.getString("email"));
                cliente.add(com);    
                System.out.println(":"+com.getApellido());
            }
            return cliente;
        } catch (Exception e) {
            return null;           
        }
    }
}
