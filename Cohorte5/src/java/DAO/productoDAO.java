/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import conexion.Conexion;
import entidades.categoria;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;
import entidades.producto;

/**
 *
 * @author rodrigo.martinezusam
 */
public class productoDAO {

    Conexion conn = new Conexion();

    public productoDAO(Conexion con) {
        this.conn = conn;
    }

    public List<producto> findAll() {
        try {
            String query = "select * from producto";
            PreparedStatement stm = this.conn.conectar().prepareStatement(query);
            ResultSet rs = stm.executeQuery();
            List<producto> producto = new LinkedList<>();

            while (rs.next()) {
                producto com = new producto();
                com.setId_producto(rs.getInt("id_producto"));
                com.setNombre(rs.getString("nombre"));
                com.setPrecio(rs.getString("precio"));
                com.setStock(rs.getInt("stock"));
                categoria ca = new categoria();
                ca.setId_categoria(1);
                com.setCategoria(ca);
                producto.add(com);

                System.out.println("::" + com.getNombre());
            }
            return producto;
        } catch (Exception e) {
            return null;
        }
    }
}
