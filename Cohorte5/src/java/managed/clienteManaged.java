/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package managed;

import conexion.Conexion;
import DAO.clienteDAO;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import entidades.cliente;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Named;

@ManagedBean(name = "clienteManagedBean")
@ViewScoped
public class clienteManaged implements Serializable {

    Conexion conn = new Conexion();
    private cliente cliente;
    private clienteDAO dao = new clienteDAO(conn);
    private List<cliente> lista;

    public Conexion getConn() {
        return conn;
    }

    public void setConn(Conexion conn) {
        this.conn = conn;
    }

    public cliente getCliente() {
        return cliente;
    }

    public void setCliente(cliente cliente) {
        this.cliente = cliente;
    }

    public clienteDAO getDao() {
        return dao;
    }

    public void setDao(clienteDAO dao) {
        this.dao = dao;
    }

    public List<cliente> getLista() {
        return lista;
    }

    public void setLista(List<cliente> lista) {
        this.lista = lista;
    }
   
    @PostConstruct

    public void init() {
        this.cliente = new cliente();
    }

    public void listar(){
        try {           
            this.dao = new clienteDAO(conn);
            this.lista = this.dao.findAll();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
