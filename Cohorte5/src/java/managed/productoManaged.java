/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package managed;

import conexion.Conexion;
import DAO.productoDAO;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import entidades.producto;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;


@ManagedBean(name = "productoManagedBean")
@ViewScoped
public class productoManaged implements Serializable {

    Conexion conn = new Conexion();
    private producto producto;
    private productoDAO dao = new productoDAO(conn);
    private List<producto> lista;

    public Conexion getConn() {
        return conn;
    }

    public void setConn(Conexion conn) {
        this.conn = conn;
    }

    public producto getProducto() {
        return producto;
    }

    public void setProducto(producto producto) {
        this.producto = producto;
    }

    public productoDAO getDao() {
        return dao;
    }

    public void setDao(productoDAO dao) {
        this.dao = dao;
    }

    public List<producto> getLista() {
        return lista;
    }

    public void setLista(List<producto> lista) {
        this.lista = lista;
    }
    
    @PostConstruct

     public void init() {
        this.producto = new producto();
    }    
    
    public void listar(){
        try {           
            this.dao = new productoDAO(conn);
            this.lista = this.dao.findAll();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
