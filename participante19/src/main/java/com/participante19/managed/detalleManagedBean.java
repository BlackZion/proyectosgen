/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.participante19.managed;

import com.participante19.ejb.detalleFacadeLocal;
import com.participante19.entity.detalle;
import com.participante19.entity.factura;
import com.participante19.entity.producto;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;

/**
 *
 * @author rodrigo.martinezusam
 */
@Named(value = "detalleManagedBean")
@SessionScoped
public class detalleManagedBean implements Serializable {

    @EJB
    private detalleFacadeLocal detalle_ejb;
    private detalle detalle;
    private factura factura;
    private producto producto;
    private List<detalle> lista;

    String Mensaje;

    public detalle getDetalle() {
        return detalle;
    }

    public void setDetalle(detalle detalle) {
        this.detalle = detalle;
    }

    public factura getFactura() {
        return factura;
    }

    public void setFactura(factura factura) {
        this.factura = factura;
    }

    public List<detalle> getLista() {
        this.lista = detalle_ejb.findAll();
        return lista;
    }

    public void setLista(List<detalle> lista) {
        this.lista = lista;
    }

    public producto getProducto() {
        return producto;
    }

    public void setProducto(producto producto) {
        this.producto = producto;
    }

    @PostConstruct

    public void init() {
        detalle = new detalle();
        factura = new factura();
        producto = new producto();
    }
    
    public void save(){
        try {
            detalle.setId_factura(factura);
            detalle.setId_producto(producto);
            detalle_ejb.create(detalle);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public void update(){
        try {
            detalle.setId_factura(factura);
            detalle.setId_producto(producto);
            detalle_ejb.edit(detalle);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public void remove(detalle deta){
        try {
            detalle_ejb.remove(deta);
        } catch (Exception e) {
        e.printStackTrace();
        }
    }
    
    public void findbyid(detalle de){
    factura.setNum_factura(de.getId_factura().getNum_factura());
    producto.setId_producto(de.getId_producto().getId_producto());
    this.detalle = de;
    }

}
