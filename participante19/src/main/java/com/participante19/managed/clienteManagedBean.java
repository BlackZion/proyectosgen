/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.participante19.managed;

import com.participante19.ejb.clienteFacadeLocal;
import com.participante19.entity.cliente;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;

@Named(value = "clienteManagedBean")
@SessionScoped
public class clienteManagedBean implements Serializable {

    @EJB
    private clienteFacadeLocal clienteejb;
    private List<cliente> lista;
    private cliente cliente;

    String mensaje;

    public List<cliente> getLista() {
        this.lista = clienteejb.findAll();
        return lista;
    }

    public void setLista(List<cliente> lista) {
        this.lista = lista;
    }

    public cliente getCliente() {
        return cliente;
    }

    public void setCliente(cliente cliente) {
        this.cliente = cliente;
    }

    @PostConstruct

    public void init() {
        cliente = new cliente();

    }

    public void save() {
        try {
            clienteejb.create(cliente);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void update() {
        try {
            clienteejb.edit(cliente);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void remove(cliente cl) {
        try {
            clienteejb.remove(cl);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void findByid(cliente c) {
        this.cliente = c;
    }

}
