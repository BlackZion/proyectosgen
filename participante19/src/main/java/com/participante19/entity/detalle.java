/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.participante19.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "detalle")
public class detalle implements Serializable{
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int num_detalle;
    
    @ManyToOne
    @JoinColumn(name = "id_factura")
    private factura id_factura;
    
    @ManyToOne
    @JoinColumn(name = "id_producto")
    private producto id_producto;
    
    @Column(name = "cantidad")
    private int cantidad;
    
    @Column(name = "precio")
    private double precio;

    public int getNum_detalle() {
        return num_detalle;
    }

    public void setNum_detalle(int num_detalle) {
        this.num_detalle = num_detalle;
    }

    public factura getId_factura() {
        return id_factura;
    }

    public void setId_factura(factura id_factura) {
        this.id_factura = id_factura;
    }

    public producto getId_producto() {
        return id_producto;
    }

    public void setId_producto(producto id_producto) {
        this.id_producto = id_producto;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + this.num_detalle;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final detalle other = (detalle) obj;
        if (this.num_detalle != other.num_detalle) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "detalle{" + "num_detalle=" + num_detalle + '}';
    }
    
}
