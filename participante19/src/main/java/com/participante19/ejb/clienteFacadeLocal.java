/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.participante19.ejb;

import com.participante19.entity.cliente;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author rodrigo.martinezusam
 */
@Local
public interface clienteFacadeLocal {

    void create(cliente cliente);

    void edit(cliente cliente);

    void remove(cliente cliente);


    List<cliente> findAll();

    
}
