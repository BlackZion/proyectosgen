/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.participante19.ejb;

import com.participante19.entity.modo_pago;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author rodrigo.martinezusam
 */
@Local
public interface modo_pagoFacadeLocal {

    void create(modo_pago modo_pago);

    void edit(modo_pago modo_pago);

    void remove(modo_pago modo_pago);

    List<modo_pago> findAll();
}
