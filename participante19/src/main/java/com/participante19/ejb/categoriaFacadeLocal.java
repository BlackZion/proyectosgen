/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.participante19.ejb;

import com.participante19.entity.categoria;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author rodrigo.martinezusam
 */
@Local
public interface categoriaFacadeLocal {

    void create(categoria categoria);

    void edit(categoria categoria);

    void remove(categoria categoria);

    List<categoria> findAll();
}
