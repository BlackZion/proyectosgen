/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.participante19.ejb;

import com.participante19.entity.factura;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author rodrigo.martinezusam
 */
@Local
public interface facturaFacadeLocal {

    void create(factura factura);

    void edit(factura factura);

    void remove(factura factura);

    List<factura> findAll();

    
}
