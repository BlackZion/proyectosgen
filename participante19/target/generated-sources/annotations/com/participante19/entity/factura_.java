package com.participante19.entity;

import com.participante19.entity.cliente;
import com.participante19.entity.modo_pago;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.0.v20130507-rNA", date="2019-10-14T14:54:20")
@StaticMetamodel(factura.class)
public class factura_ { 

    public static volatile SingularAttribute<factura, Date> fecha;
    public static volatile SingularAttribute<factura, cliente> id_cliente;
    public static volatile SingularAttribute<factura, modo_pago> num_pago;
    public static volatile SingularAttribute<factura, Integer> num_factura;

}