/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entidades;

import java.io.Serializable;

/**
 *
 * @author rodrigo.martinezusam
 */

public class modo_pago implements Serializable{
    
    private int num_pago;
    
    private String nombre;
    
    private String otros_detalles;

    public int getNum_pago() {
        return num_pago;
    }

    public void setNum_pago(int num_pago) {
        this.num_pago = num_pago;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getOtros_detalles() {
        return otros_detalles;
    }

    public void setOtros_detalles(String otros_detalles) {
        this.otros_detalles = otros_detalles;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 79 * hash + this.num_pago;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final modo_pago other = (modo_pago) obj;
        if (this.num_pago != other.num_pago) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "modo_pago{" + "num_pago=" + num_pago + '}';
    }
    
}
