/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entidades;

import java.io.Serializable;

/**
 *
 * @author rodrigo.martinezusam
 */
public class detalle implements Serializable {

    private int num_detalle;

    private factura id_factura;

    private producto id_producto;

    private int cantidad;

    private double precio;

    public int getNum_detalle() {
        return num_detalle;
    }

    public void setNum_detalle(int num_detalle) {
        this.num_detalle = num_detalle;
    }

    public factura getId_factura() {
        return id_factura;
    }

    public void setId_factura(factura id_factura) {
        this.id_factura = id_factura;
    }

    public producto getId_producto() {
        return id_producto;
    }

    public void setId_producto(producto id_producto) {
        this.id_producto = id_producto;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + this.num_detalle;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final detalle other = (detalle) obj;
        if (this.num_detalle != other.num_detalle) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "detalle{" + "num_detalle=" + num_detalle + '}';
    }
    
    
}
