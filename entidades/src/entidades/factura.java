/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entidades;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author rodrigo.martinezusam
 */
public class factura implements Serializable{
    
    private int num_factura;
    
    private cliente id_cliente;
    
    private Date fecha;
    
    private modo_pago num_pago;

    public int getNum_factura() {
        return num_factura;
    }

    public void setNum_factura(int num_factura) {
        this.num_factura = num_factura;
    }

    public cliente getId_cliente() {
        return id_cliente;
    }

    public void setId_cliente(cliente id_cliente) {
        this.id_cliente = id_cliente;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public modo_pago getNum_pago() {
        return num_pago;
    }

    public void setNum_pago(modo_pago num_pago) {
        this.num_pago = num_pago;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 97 * hash + this.num_factura;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final factura other = (factura) obj;
        if (this.num_factura != other.num_factura) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "factura{" + "num_factura=" + num_factura + '}';
    }
    
     
}
